#define __TBB_show_deprecation_message_atomic_H
#define __TBB_show_deprecation_message_task_H

#include "LOP_3DelightStandardRenderVars.h"

#include "lop_utilities.h"
#include "ui/aov.h"

#include <LOP/LOP_PRMShared.h>
#include <LOP/LOP_Error.h>
#include <OP/OP_OperatorTable.h>
#include <OP/OP_Operator.h>
#include <OP/OP_OpTypeId.h>
#include <PRM/PRM_Include.h>
#include <PRM/PRM_SpareData.h>
#include <PRM/PRM_Parm.h>
#include <SYS/SYS_Version.h>
#include <UT/UT_HDKVersion.h>

PRM_Template* LOP_3DelightStandardRenderVars::GetTemplates()
{    

    static std::vector<PRM_Template> myTemplateList = {
    PRM_Template(),
    };

    return &myTemplateList[0];
}

void
LOP_3DelightStandardRenderVars::Register(OP_OperatorTable* io_table)
{
    OP_Operator *op = new OP_Operator(
        "3delightstandardrendervars",
        "3Delight Standard Render Vars",
        LOP_3DelightStandardRenderVars::alloc,
        LOP_3DelightStandardRenderVars::GetTemplates(),
        LOP_SubNet::theChildTableName,
        0, 1, nullptr,
        OP_FLAG_GENERATOR | OP_FLAG_NETWORK);
    
    op->setIconName("ROP_3Delight");
  
    io_table->addOperator(op);
}

OP_Node *
LOP_3DelightStandardRenderVars::alloc(
	OP_Network *net, const char *name, OP_Operator *op)
{
    return new LOP_3DelightStandardRenderVars(net, name, op);
}

const char *
LOP_3DelightStandardRenderVars::getChildType() const
{
    return OPtypeIdToString(LOP_OPTYPE_ID);
}

OP_OpTypeId
LOP_3DelightStandardRenderVars::getChildTypeID() const
{
    return LOP_OPTYPE_ID;
}

LOP_3DelightStandardRenderVars::LOP_3DelightStandardRenderVars(
	OP_Network *net, const char *name, OP_Operator *op)
:
	LOP_SubNet(net, name, op)
{
}

LOP_3DelightStandardRenderVars::~LOP_3DelightStandardRenderVars()
{
}

bool
LOP_3DelightStandardRenderVars::runCreateScript()
{
	bool ret = LOP_SubNet::runCreateScript();

    const auto& descriptions = aov::getDescriptions();
    OP_Node *prev_node = nullptr; 

    for (const auto& desc : descriptions)
	{
		/* Build a clean version of the name. This matches how 3Delight names
		   the channel in an exr so we can use it for that too. */
		std::string clean_var_name = desc.m_variable_name;
		std::replace(clean_var_name.begin(), clean_var_name.end(), '.', '_');

        //Create a render var node for each aov.
        OP_Node *render_var_node = createNode(
			"rendervar", clean_var_name.c_str());
		render_var_node->runCreateScript();

		/* HdNSI accepts "source:variable_name" encoding and will default to
		   "shader" source if there is only a name. */
		render_var_node->setString(
			desc.m_variable_source + ":" + desc.m_variable_name,
			CH_STRING_LITERAL, "sourceName", 0, 0);

		/* Name the channel for exr files, matching 3Delight's behavior. */
		if( clean_var_name != "Ci" )
		{
			setRenderVarPrefix(
				render_var_node, clean_var_name, CH_STRING_LITERAL);
		}

		/* This logic is not exhaustive. It covers what we need for now. */
		const char *datatype = "color3f";
		const char *format = "color3h";
		if( desc.m_with_alpha )
		{
			datatype = "color4f";
			format = "color4h";
		}
		else if( desc.m_layer_type == "vector" )
		{
			format = "float3";
		}
		else if( desc.m_layer_type == "scalar" )
		{
			datatype = "float";
			format = "float";
		}
		render_var_node->setString(
			datatype, CH_STRING_LITERAL, "dataType", 0, 0);
		render_var_node->setString(
			format, CH_STRING_LITERAL,
			"xn__driverparametersaovformat_shbkd", 0, 0);

		lop_utilities::ConnectInput(render_var_node, prev_node);
        prev_node = render_var_node;
    }
    prev_node->setDisplay(true);
	lop_utilities::LayoutNodes(this);
    finishedLoadingNetwork();
	return ret;
}

void LOP_3DelightStandardRenderVars::setRenderVarPrefix(
	OP_Node *render_var_node,
	const UT_StringRef &val,
	CH_StringMeaning meaning)
{
#if SYS_VERSION_MAJOR_INT >= 20
	render_var_node->setString(
		"set", CH_STRING_LITERAL,
		"xn__driverparametersaovhuskchannel_prefix_control_dfckde", 0, 0);
	render_var_node->setString(
		val, meaning,
		"xn__driverparametersaovhuskchannel_prefix_c1bkde", 0, 0);
#else
	render_var_node->setString(
		val, meaning,
		"xn__driverparametersaovchannel_prefix_tubkd", 0, 0);
#endif
}

OP_ERROR
LOP_3DelightStandardRenderVars::cookMyLop(OP_Context &context)
{
#if HDK_API_VERSION >= 19050000
    syncDelayedOTL();
    UT_ASSERT(!getDelaySyncOTL());
#endif
    return cookReferenceNode(context, getNodeForSubnetOutput(0));
}
