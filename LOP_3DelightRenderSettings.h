#ifndef __LOP_3DelightRenderSettings_h__
#define __LOP_3DelightRenderSettings_h__
#include <LOP/LOP_SubNet.h>

class LOP_3DelightRenderSettings : public LOP_SubNet
{
public:
    static void Register(OP_OperatorTable* io_table);

    LOP_3DelightRenderSettings(OP_Network *net, const char *name, OP_Operator *op);
    ~LOP_3DelightRenderSettings() override;
    static PRM_Template  myTemplateList[];
    static OP_Node *alloc(OP_Network *net, const char *name,
                                OP_Operator *op);
    const char *getChildType() const override;
    OP_OpTypeId getChildTypeID() const override;

    /**
		Called when a new node is created.
	*/
	virtual bool runCreateScript() override;
	static PRM_Template* GetTemplates();

	void DoRender(double i_time);

public:
    //Parameters
    static const char* k_camera;
	static const char* k_resolution;
	static const char* k_idisplay_rendering;

    /* Generate VEX string for RenderProduct orderedVars. */
    static std::string getRenderProductOrderedVarsString();

    static int refreshMultiLightCB(
        void* data, int index, fpreal t, const PRM_Template* tplate);

protected:
    /// Method to cook USD data for the LOP
    OP_ERROR cookMyLop(OP_Context &context) override;

    // Used to enable some buttons.
    bool updateParmsFlags() override;

private:
    void PRIMPATH(UT_String&str, fpreal t);
    void getCamera(UT_String&str, fpreal t);
	OP_Node* createCustomVarsNetwork();
	OP_Node* createMultilightVarsNetwork();
	void updateMultiLight();
};
#endif
