#include "ROP_3DelightOcean.h"

#include "../camera.h"
#include "../context.h"
#include "../null.h"
#include "../nsi_loader.h"
#include "OceanContext.h"
#include "../ui/settings.h"

#include <OP/OP_Director.h>
#include <OP/OP_Operator.h>
#include <OP/OP_OperatorTable.h>
#include <PRM/PRM_Conditional.h>
#include <PRM/PRM_Range.h>
#include <PRM/PRM_SpareData.h>

#include <3Delight/OceanBake.h>

#include <mutex>
#include <vector>

void ROP_3DelightOcean::Register(OP_OperatorTable *io_table)
{
	OP_Operator *op = new OP_Operator(
		"3DelightBakeOcean",
		"3Delight Bake Ocean",
		ROP_3DelightOcean::alloc,
		GetTemplates(),
		0,
		OP_MULTI_INPUT_MAX,
		(CH_LocalVariable*)nullptr,
		0, /* flags */
		nullptr, /* input labels */
		1, /* max outputs */
		"Render" /* submenu */);

	op->setIconName("ROP_3Delight");
	op->setObsoleteTemplates(GetObsoleteTemplates());
	io_table->addOperator(op);
}

OP_Node* ROP_3DelightOcean::alloc(
	OP_Network *net,
	const char *name,
	OP_Operator *op)
{
	return new ROP_3DelightOcean(net, name, op);
}

ROP_3DelightOcean::ROP_3DelightOcean(
	OP_Network *net, const char *name, OP_Operator *op)
:
	ROP_Node(net, name, op)
{
}

int ROP_3DelightOcean::startRender(
	int nframes, fpreal tstart, fpreal tend)
{
	return 1;
}

ROP_RENDER_CODE ROP_3DelightOcean::renderFrame(
	fpreal time, UT_Interrupt *boss)
{
	static auto myDlBakeOcean =
		GetNSIAPI().LoadFunction<decltype(&DlBakeOcean)>("DlBakeOcean");

	if( !myDlBakeOcean )
	{
		return ROP_ABORT_RENDER;
	}

	UT_String output_texture;
	evalString(output_texture, "output_texture", 0, time);

	UT_String filename;
	evalString(filename, "ocean_spectrum", 0, time);
	if( !filename.startsWith("op:") && findNode(filename) )
	{
		/* Convert this automatically so the OP picker just works. */
		filename.prepend("op:");
	}

	UT_String maskname;
	evalString(maskname, "ocean_mask", 0, time);
	if( !maskname.startsWith("op:") && findNode(maskname) )
	{
		maskname.prepend("op:");
	}

	OceanContext::ConstantInputs inputs;
	inputs.m_filename.emplace_back(filename);
	inputs.m_maskname.emplace_back(maskname);
	inputs.m_time = time;
	inputs.m_compute_cusp = evalInt("bake_cusp", 0, time);
	inputs.m_with_antialiasing = evalInt("use_antialiasing", 0, time);

	double plane_size[2] =
	{
		evalFloat("ocean_plane_size", 0, time),
		evalFloat("ocean_plane_size", 1, time)
	};

	double plane_center[3] =
	{
		evalFloat("ocean_plane_center", 0, time),
		evalFloat("ocean_plane_center", 1, time),
		evalFloat("ocean_plane_center", 2, time)
	};

	int bake_res = evalInt("output_resolution", 0, time);
	double min_camera_distance = evalFloat("minimum_camera_distance", 0, time);
	int use_frustum = evalInt("use_camera_frustum", 0, time);

	class HoudiniOceanEvaluator : public DlOceanEvaluator
	{
	public:
		HoudiniOceanEvaluator(
			OceanContext::ConstantInputs &i_ctx_inputs,
			OP_Node *i_node)
		:
			m_ctx_inputs{i_ctx_inputs},
			m_node{i_node}
		{
			/* This value was picked for giving the best performance. */
			m_max_evaluation_size = 1024;
		}

		void GetInputBuffers(
			float **position,
			float **positionfilterwidth) override
		{
			std::shared_ptr<OceanContext> ctx = GetCtx();
			*position = ctx->m_P.get();
			*positionfilterwidth = ctx->m_dP.get();
		}

        virtual void Evaluate(
			unsigned evaluation_size,
			const float **displacement_vector,
			const float **cusp) override
		{
			assert(evaluation_size <= m_max_evaluation_size);
			std::shared_ptr<OceanContext> ctx = GetCtx();
			ctx->Run(evaluation_size);
			*displacement_vector = ctx->m_disp_vector.get();
			*cusp = ctx->m_cusp.get();
		}

		std::shared_ptr<OceanContext> GetCtx()
		{
			thread_local std::weak_ptr<OceanContext> eval_ctx;

			std::shared_ptr<OceanContext> my_context = eval_ctx.lock();
			if( !my_context || !my_context->HasSameInputs(m_ctx_inputs) )
			{
				std::lock_guard ctx_list_l{m_ctx_list_mutex};
				/*
					If the ocean spectrum is an op: reference, Houdini has a
					tendency to deadlock if we do CVEX_Context init in multiple
					threads. Which is why this is also inside the mutex which
					guards the list. All but the first load() are very fast so
					it doesn't hurt anything.
				*/
				my_context = std::make_shared<OceanContext>(
					m_max_evaluation_size, m_ctx_inputs, m_node);
				/* Save a strong ref until all baking is done here. */
				eval_ctx_list.emplace_back(my_context);
				/* Save weak ref, we'll reuse the ctx in this thread. */
				eval_ctx = my_context;
			}
			return my_context;
		}

		/* Inputs shared by all contexts. */
		OceanContext::ConstantInputs &m_ctx_inputs;
		/* Node doing evaluation. */
		OP_Node *m_node;
		/* Mutex for the context list. */
		std::mutex m_ctx_list_mutex;
		/* The list of used contexts. */
		std::vector<std::shared_ptr<OceanContext>> eval_ctx_list;
	};
	HoudiniOceanEvaluator evaluator{inputs, this};

	/*
		VEX has a tendency to deadlock if not first run in the main thread to
		load the shader and any referenced geo (and perhaps particularly op
		references). This is what we do here. It will then work fine with
		multithreading.
	*/
	{
		float *dummypos, *dummyfilter;
		evaluator.GetInputBuffers(&dummypos, &dummyfilter);
		dummypos[0] = 0.0f;
		dummypos[1] = 0.0f;
		dummypos[2] = 0.0f;
		dummyfilter[0] = 0.0f;
		const float *out, *out2;
		evaluator.Evaluate(1, &out, &out2);
	}

	std::unique_ptr<NSI::Context> camera_ctx = ExportCamera(time);

	myDlBakeOcean(
		evaluator,
		output_texture.c_str(),
		camera_ctx->Handle(),
		min_camera_distance,
		use_frustum ? 0.8f : 0.0f,
		plane_size,
		plane_center,
		bake_res,
		inputs.m_compute_cusp);

	return ROP_CONTINUE_RENDER;
}

ROP_RENDER_CODE ROP_3DelightOcean::endRender()
{
	return ROP_CONTINUE_RENDER;
}

/*
	Manually export just the camera to a NSI context.
*/
std::unique_ptr<NSI::Context> ROP_3DelightOcean::ExportCamera(
	fpreal time) const
{
	std::unique_ptr<NSI::Context> nsi_ctx;
	nsi_ctx.reset(new NSI::Context(GetNSIAPI()));
	nsi_ctx->Begin();

	/* This is missing a lot of pieces but it's enough for our use. */
	context export_ctx(
		this,
		nullptr, /* no ROP_3Delight */
		*nsi_ctx,
		*nsi_ctx,
		time,
		time,
		0.0,
		0.0,
		false, /* dof */
		false, /* batch */
		false, /* ipr */
		false, /* export nsi */
		{},
		standard);

	OBJ_Node *camera_node = settings::get_obj_camera(this, time);

	if( camera_node )
	{
		/* Find required transform nodes. See null::connect(). */
		std::vector<std::unique_ptr<exporter>> exporters;
		/* Scan parent objects. */
		for( OBJ_Node *obj = camera_node; obj; obj = obj->getParentObject() )
		{
			exporters.emplace_back(std::make_unique<null>(export_ctx, obj));
		}
		/* Scan parents. */
		for( OBJ_Node *obj = camera_node; true; )
		{
			OP_Node *parent = obj->getParent();
			if( !parent )
				break;
			obj = parent->castToOBJNode();
			if( !obj )
				break;
			exporters.emplace_back(std::make_unique<null>(export_ctx, obj));
		}
		/* And finally the camera exporter. */
		exporters.emplace_back(
			std::make_unique<camera>(export_ctx, camera_node));
		/* Now run all of these, like scene::export_nsi(). */
		for( const auto &e : exporters )
		{
			e->create();
		}
		for( const auto &e : exporters )
		{
			e->connect();
		}
		for( const auto &e : exporters )
		{
			e->set_attributes();
		}
	}

	return nsi_ctx;
}

PRM_Template* ROP_3DelightOcean::GetTemplates()
{
	static std::vector<PRM_Template> templates;
	if( !templates.empty() )
	{
		/* This should only be called once. But it is static content anyway so
		   we return it again just in case. */
		assert(false);
		return templates.data();
	}

	/* Add the default ROP UI from ROP_Node. */
	for( PRM_Template *base = getROPbaseTemplate();
	     base->getType() != PRM_LIST_TERMINATOR; ++base )
	{
		templates.emplace_back(*base);
	}

	{
		static PRM_Name name(settings::k_camera, "Camera");
		static PRM_Default def(0.0f, "/obj/cam1");
		const char *help =
			"When a camera is specified, the ocean texture is written with more"
			" detail close to the camera, to provide optimal quality when"
			" viewed from the camera's location.\n\n"
			"Without a camera, uniform detail is used across the entire ocean. "
			"This can be suitable for small oceans.";
		templates.emplace_back(
			PRM_STRING, PRM_TYPE_DYNAMIC_PATH, 1, &name, &def,
			nullptr, nullptr, PRM_Callback{}, &PRM_SpareData::objCameraPath,
			1, help);
	}
	{
		static PRM_Name name(
			"minimum_camera_distance", "Minimum Distance");
		static PRM_Default def(1.0f);
		const char *help =
			"Consider the camera to always be at least this far from the ocean "
			"for spreading detail level. The main purpose is to avoid trying "
			"to capture more detail near the camera than is available.\n\n"
			"A larger value can be used to get a texture which is arbitrarily "
			"less camera centric.";
		templates.emplace_back(
			PRM_FLT | PRM_Type(PRM_Type::PRM_INTERFACE_PLAIN), 1, &name, &def,
			nullptr, nullptr, PRM_Callback{}, nullptr, 1, help);
	}
	{
		static PRM_Name name("use_camera_frustum", "Focus On Camera Frustum");
		static PRM_Default def(1.0f);
		const char *help =
			"When enabled, most available texture detail is used for the part "
			"of the ocean visible through the camera. When disabled, the "
			"camera's position is still used but its orientation is not.";
		templates.emplace_back(
			PRM_TOGGLE, 1, &name, &def, nullptr, nullptr, PRM_Callback{},
			nullptr, 1, help);
	}
	{
		static PRM_Name name(settings::k_camera_overscan, "Camera Overscan %");
		static PRM_Default def[2] = {0.0f, 0.0f};
		static PRM_Range range(
			PRM_RANGE_RESTRICTED, 0.0, PRM_RANGE_RESTRICTED, 100.0);
		const char *help =
			"Amount of overscan added to camera frustum when baking."
			" Horizontal and vertical %.";
		static PRM_Conditional disable_cond("{use_camera_frustum == 0}");
		templates.emplace_back(
			PRM_FLT | PRM_Type(PRM_Type::PRM_INTERFACE_PLAIN), 2, &name, def,
			nullptr, &range, PRM_Callback{}, nullptr, 1, help, &disable_cond);
	}
	{
		static PRM_Name name("bake_cusp", "Bake Cusp");
		static PRM_Default def(0.0f);
		const char *help =
			"Bake the cusp attribute into the alpha channel of the texture. "
			"The texture does not have an alpha channel otherwise. "
			"Read this using the Ocean Cusp shader (dlOceanCusp).";
		templates.emplace_back(
			PRM_TOGGLE, 1, &name, &def, nullptr, nullptr, PRM_Callback{},
			nullptr, 1, help);
	}
	{
		static PRM_Name name("use_antialiasing", "Anti-aliasing");
		static PRM_Default def(0.0f);
		const char *help =
			"This helps filter out noise near the horizon of a large ocean, "
			"at the expense of a slower bake. Render time is not affected. "
			"It will also slightly reduce the amount of detail when the "
			"texture resolution is too low. "
			"\nNote that the performance penalty is much greater with Houdini "
			"versions before 20.";
		templates.emplace_back(
			PRM_TOGGLE, 1, &name, &def, nullptr, nullptr, PRM_Callback{},
			nullptr, 1, help);
	}
	{
		static PRM_Name name("ocean_plane_size", "Ocean Plane Size");
		static PRM_Default def[2] = {50.f, 50.0f};
		static PRM_Range range(PRM_RANGE_RESTRICTED, 0.f, PRM_RANGE_UI, 100.f);
		templates.emplace_back(
			PRM_FLT | PRM_Type(PRM_Type::PRM_INTERFACE_PLAIN), 2, &name,
			&def[0], nullptr, &range);
	}
	{
		static PRM_Name name("ocean_plane_center", "Ocean Plane Center");
		static PRM_Default def[3] = {0.0f, 0.0f, 0.0f};
		templates.emplace_back(
			PRM_XYZ, 3, &name, &def[0]);
	}
	{
		static PRM_Name name("output_texture", "Output Texture");
		static PRM_Default def(0.0f, "$HIP/render/`$HIPNAME`_ocean_$F4.tdl");
		const char *help =
			"Path of the texture to write. It must then be used with the ocean "
			"displacement shader.";
		templates.emplace_back(
			PRM_FILE, 1, &name, &def, nullptr, nullptr, nullptr,
			&PRM_SpareData::fileChooserModeWrite, 1, help);
	}
	{
		static PRM_Name name{"output_resolution", "Output Resolution"};
		static PRM_Default def(2048.0f);
		static PRM_Range range(PRM_RANGE_RESTRICTED, 1.f, PRM_RANGE_UI, 4096.f);
		templates.emplace_back(
			PRM_INT | PRM_Type(PRM_Type::PRM_INTERFACE_PLAIN), 1, &name, &def,
			nullptr, &range);
	}
	{
		static PRM_Name name("ocean_spectrum", "Ocean spectrum");
		static PRM_Default def(0.0f, "$HIP/geo/$HIPNAME.spectra.bgeo.sc");
		const char *help = "Saved spectrum file or path to spectrum sop.";
		templates.emplace_back(
			PRM_STRING, PRM_TYPE_DYNAMIC_PATH, 1, &name, &def,
			nullptr, nullptr, PRM_Callback{}, &PRM_SpareData::sopPath, 1, help);
	}
	{
		static PRM_Name name("ocean_mask", "Ocean mask");
		static PRM_Default def(0.0f, "");
		const char *help = "Saved mask file or path to mask sop.";
		templates.emplace_back(
			PRM_FILE, 1, &name, &def, nullptr, nullptr, nullptr,
			&PRM_SpareData::fileChooserModeRead, 1, help);
	}

    templates.emplace_back(PRM_LIST_TERMINATOR);
	return templates.data();
}

PRM_Template* ROP_3DelightOcean::GetObsoleteTemplates()
{
	static std::vector<PRM_Template> templates;
	if( !templates.empty() )
	{
		/* This should only be called once. But it is static content anyway so
		   we return it again just in case. */
		assert(false);
		return templates.data();
	}

	{
		static PRM_Name name("camera_overscan");
		static PRM_Default def(0.0f);
		templates.emplace_back(PRM_FLT, 1, &name, &def);
	}

    templates.emplace_back(PRM_LIST_TERMINATOR);
	return templates.data();
}

// vim: set softtabstop=0 noexpandtab shiftwidth=4 tabstop=4:
