#ifndef __ROP_3DelightOcean_h
#define __ROP_3DelightOcean_h

#include <ROP/ROP_Node.h>

#include <nsi.hpp>

#include <memory>

class ROP_3DelightOcean : public ROP_Node
{
public:
	static void Register(OP_OperatorTable *io_table);

	static OP_Node* alloc(OP_Network *net, const char *name, OP_Operator *op);

	ROP_3DelightOcean(OP_Network *net, const char *name, OP_Operator *op);

	int startRender(int nframes, fpreal tstart, fpreal tend) override;
	ROP_RENDER_CODE renderFrame(fpreal time, UT_Interrupt *boss = 0) override;
	ROP_RENDER_CODE endRender() override;

private:
	std::unique_ptr<NSI::Context> ExportCamera(fpreal time) const;

private:
	static PRM_Template* GetTemplates();
	static PRM_Template* GetObsoleteTemplates();
};

#endif
// vim: set softtabstop=0 noexpandtab shiftwidth=4 tabstop=4:
