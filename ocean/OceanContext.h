#ifndef OceanContext_h
#define OceanContext_h

#include "../dl_system.h"

#include <memory>

#include <CVEX/CVEX_Context.h>
#include <OP/OP_Caller.h>

class OceanContext : public CVEX_Context
{
public:
	struct ConstantInputs
	{
		CVEX_StringArray m_filename;
		CVEX_StringArray m_maskname;
		float m_time;
		/* Conceptually a shader input, if not in fact (see below). */
		bool m_compute_cusp;
		bool m_with_antialiasing{false};
	};

	/**
		\param i_buffer_size
			Maximum size which can be used with Run().
		\param i_inputs
			Constant inputs of the shader.
		\param i_evaluating_node
			The node doing evaluation.
	*/
	OceanContext(
		size_t i_buffer_size,
		ConstantInputs &i_inputs,
		OP_Node *i_evaluating_node)
	:
		m_inputs{i_inputs},
		m_caller{i_evaluating_node, nullptr, {}}
	{
		/*
			The OpCaller is what will tell Houdini to clean up the data cached
			for op: references. Without this, changes to the referenced node
			are not visible in later evaluations.
		*/
		m_rundata.setOpCaller(&m_caller);

		/*
			The code being run here doesn't really benefit from
			addConstantInput() so we don't bother. In fact, it's a downside as
			when the inputs do change between context, we'll have to pay load
			time again.
		*/
		addInput("filename", i_inputs.m_filename);
		addInput("maskname", i_inputs.m_maskname);
		addInput("time", CVEX_TYPE_FLOAT, &i_inputs.m_time, 1);

		m_P.reset(new float[3 * i_buffer_size]);
		std::fill_n(m_P.get(), 3 * i_buffer_size, 0.0f);
		addInput("P", CVEX_TYPE_VECTOR3, m_P.get(), i_buffer_size);

		m_dP.reset(new float[i_buffer_size]);
		std::fill_n(m_dP.get(), i_buffer_size, 0.0f);
		/*
			Don't set dP at all when antialiasing is turned off. This is needed
			to avoid the VEX performance penalty from making it a varying.
			Merely setting all values to 0 is not good enough.
		*/
		if( m_inputs.m_with_antialiasing )
		{
			addInput("dP", CVEX_TYPE_FLOAT, m_dP.get(), i_buffer_size);
		}

		addRequiredOutput("len", CVEX_TYPE_FLOAT);
		addRequiredOutput("displacement_vector", CVEX_TYPE_VECTOR3);
		/*
			Not setting this when we don't need it lets VEX optimize away the
			computeCusp() call in the shader and even the calculation of the
			jacobian inside oceanSampleLayers(). The whole shader is about 60%
			slower with cusp (20% for just the jacobian) so it's important.
		*/
		if( m_inputs.m_compute_cusp )
		{
			addRequiredOutput("cusp", CVEX_TYPE_FLOAT);
		}

		/*
			Init takes a while the first time (125-175ms here) but is 0ms after
			that. The compiled VEX is cached, which is apparent as edits to the
			source are not reflected until a Houdini restart.
		*/
		std::string shader_path = dl_system::dir_name(dl_system::library_path())
			+ "/../vex_shaders/ocean_run.vfl";
		const char *argv[] = { shader_path.c_str(), nullptr };
		bool ret = load(1, argv);
		if( !ret )
		{
			fprintf(stderr,
				"error loading ocean_run.fvl\n%s\n%s\n%s\n",
				getLastError(),
				getVexErrors().c_str(),
				getVexWarnings().c_str());
		}

		CVEX_Value *disp_vector_val =
			findOutput("displacement_vector", CVEX_TYPE_VECTOR3);
		if( disp_vector_val )
		{
			m_disp_vector.reset(new float[3 * i_buffer_size]);
			disp_vector_val->setRawData(
				CVEX_TYPE_VECTOR3, m_disp_vector.get(), i_buffer_size);
		}

		CVEX_Value *cusp_val = findOutput("cusp", CVEX_TYPE_FLOAT);
		if( cusp_val )
		{
			m_cusp.reset(new float[i_buffer_size]);
			cusp_val->setRawData(CVEX_TYPE_FLOAT, m_cusp.get(), i_buffer_size);
		}
	}

	/* This check is overkill for now. Just in case we use really strange
	   multithreading in the future. */
	bool HasSameInputs(ConstantInputs &i_inputs) const
		{ return &m_inputs == &i_inputs; }

	bool Run(int array_size)
	{
		return CVEX_Context::run(array_size, false, &m_rundata);
	}

	ConstantInputs &m_inputs;
	std::unique_ptr<float[]> m_P;
	std::unique_ptr<float[]> m_dP;
	std::unique_ptr<float[]> m_disp_vector;
	std::unique_ptr<float[]> m_cusp;

private:
	OP_Caller m_caller;
	CVEX_RunData m_rundata;
};

#endif
// vim: set softtabstop=0 noexpandtab shiftwidth=4 tabstop=4:
