#define __TBB_show_deprecation_message_atomic_H
#define __TBB_show_deprecation_message_task_H

#include "LOP_3DelightRenderSettings.h"

#include "LOP_3DelightStandardRenderVars.h"
#include "lop_utilities.h"
#include "ui/aov.h"
#include "ui/settings.h"

#include <HUSD/HUSD_Utils.h>
#include <LOP/LOP_PRMShared.h>
#include <LOP/LOP_Error.h>
#include <OP/OP_OperatorTable.h>
#include <OP/OP_Operator.h>
#include <OP/OP_OpTypeId.h>
#include <PRM/PRM_Include.h>
#include <PRM/PRM_Parm.h>
#include <UT/UT_HDKVersion.h>

const char* LOP_3DelightRenderSettings::k_camera = "camera";
const char* LOP_3DelightRenderSettings::k_resolution = "resolution";
static constexpr unsigned kNumCustomAOVSlots = 8;

/*
	Callback to make the output toggles behave like a radio button.
*/
static int OutputRadioCallback(
	void *data, int index, fpreal t, const PRM_Template *tplate)
{
	OP_Parameters *node = reinterpret_cast<OP_Parameters*>(data);
	const UT_StringRef &toggled = tplate->getNamePtr()->getTokenRef();
	if( node->evalInt(toggled, 0, t) )
	{
		const char *all_options[] = {
			"display_rendered_images",
			"save_rendered_images",
			"export_nsi_file",
		};
		for( const char *option : all_options )
		{
			if( toggled != option )
			{
				/* Unset all other options. */
				node->setInt(option, 0, t, false);
			}
		}
	}
	else
	{
		/* Unselecting is not allowed. */
		node->setInt(toggled, 0, t, true);
	}
	return 1;
}

static int RenderCB(
	void *i_node, int i_index, double i_time, const PRM_Template *i_template)
{
	reinterpret_cast<LOP_3DelightRenderSettings*>(i_node)->DoRender(i_time);
	return 0; /* no need to refresh dialog */
}

std::string LOP_3DelightRenderSettings::getRenderProductOrderedVarsString()
{
	static std::string str;
	if (!str.empty())
	{
		return str;
	}

	/* Build an expression for render products. */
	const auto& descriptions = aov::getDescriptions();
	for (const auto& desc : descriptions)
	{
		std::string label_name = desc.m_variable_name;
		std::replace(label_name.begin(), label_name.end(), '.', '_');
		str += "`ifs(ch(\"../" + label_name + "\"), \"/Render/Products/Vars/" + label_name + "\", \"\")` ";
	}
	for( unsigned i = 1; i <= kNumCustomAOVSlots; ++i )
	{
		std::string node_name = "custom_aov" + std::to_string(i);
		std::string parm_name = "../custom_aov_" + std::to_string(i);
		str += "`ifs(0 != strlen(chs(\"" + parm_name +
			"\")), \"/Render/Products/Vars/" + node_name + "\", \"\")` ";
	}

	return str;
}

int LOP_3DelightRenderSettings::refreshMultiLightCB(
	void* data, int index, fpreal t, const PRM_Template* tplate)
{
	reinterpret_cast<LOP_3DelightRenderSettings*>(data)->updateMultiLight();
	return 1;
}

void LOP_3DelightRenderSettings::updateMultiLight()
{
	int nb_lights = evalInt(settings::k_light_sets, 0, 0.0f);
	PRM_Parm &param = getParm(settings::k_light_sets);

	std::vector<std::string> layer_names;

	/* Get selection. */
	UT_String multi_light_selection_val;
	evalString(multi_light_selection_val, settings::k_multi_light_selection, 0, 0.0f);
	if (multi_light_selection_val == "off")
	{
		/* Delete all lights. */
		for ( int i = 0; i < nb_lights; i++)
		{
			param.removeMultiParmItem(nb_lights - 1 - i);
		}
		param.insertMultiParmItem(0);
	}
	else if (multi_light_selection_val == "tagged")
	{
		HUSD_AutoWriteLock writelock(editableDataHandle());
		HUSD_AutoLayerLock layerlock(writelock);

		/* Find LPE tags. */
		std::unordered_map<std::string, std::string> primpath_tags;
		lop_utilities::FindLPETags(this, layerlock, primpath_tags);

		/* Find LOP light nodes. */
		std::vector<std::string> light_primpaths;
		lop_utilities::FindLights(this, layerlock, light_primpaths);

		/* Calculate layer name, it should be either tag name or light name. */
		for ( const std::string &light_primpath : light_primpaths )
		{
			std::string layer_name(light_primpath);
			auto primpath_tags_itr = primpath_tags.find(layer_name);
			if (primpath_tags_itr != primpath_tags.end())
			{
				const std::string &tag = primpath_tags_itr->second;
				layer_name = tag;
			}
			if (std::find(layer_names.begin(), layer_names.end(), layer_name) == layer_names.end())
			{
				layer_names.push_back(layer_name);
			}
		}

		/* Update light list. */
		for ( int i = 0; i < nb_lights; i++)
		{
			param.removeMultiParmItem(nb_lights - 1 - i);
		}
		for ( size_t i = 0; i < layer_names.size(); i++ )
		{
			param.insertMultiParmItem(i);
		}
		for ( size_t i = 0; i < layer_names.size(); i++ )
		{
			const std::string &layer_name = layer_names[i];
			const std::string &light_token = settings::GetLightToken(i);
			setString(layer_name.c_str(), CH_STRING_LITERAL, light_token.c_str(), 0, 0.0f);
		}
	}

	/* Update multilight_vars subnet. */
	LOP_Node *multilight_vars = CAST_LOPNODE(findNode("multilightvars"));
	if (!multilight_vars)
	{
		/* Likely old render settings created without multilight support. */
		return;
	}

	/* Delete all RenderVar nodes. */
	OP_NodeList multilight_vars_child_nodes;
	multilight_vars->getAllChildren(multilight_vars_child_nodes);

	OP_NodeList layer_nodes;
	for (OP_Node *child_node : multilight_vars_child_nodes)
	{
		if (child_node->isOutputNode())
		{
			continue;
		}
		layer_nodes.append(child_node);
	}
	multilight_vars->destroyNodes(layer_nodes);

	/* Create N*M RenderVars nodes.
	   N = number of enabled shading components, M = number of layers */
	std::vector<aov::description> aov_descs;
	for (const auto& desc : aov::getDescriptions())
	{
		if (!desc.m_support_multilight)
		{
			continue;
		}
		std::string parm_name = desc.m_variable_name;
		std::replace(parm_name.begin(), parm_name.end(), '.', '_');
		int enabled = evalInt(parm_name, 0, 0.0f);
		if (enabled)
		{
			aov_descs.push_back(desc);
		}
	}

	OP_Node *prev_node = nullptr;
	std::vector<std::string> node_names;
	for ( size_t i = 0; i < layer_names.size(); i++ )
	{
		const std::string &layer_name = layer_names[i];
		for ( size_t j = 0; j < aov_descs.size(); j++ )
		{
			const aov::description &desc = aov_descs[j];

			std::string aov_name = desc.m_variable_name;
			std::replace(aov_name.begin(), aov_name.end(), '.', '_');
			aov_name = aov_name + "_" + layer_name;

			/* Append LPE tag to source name. */
			std::string source_name =
				desc.m_variable_source + ":" + desc.m_variable_name;
			source_name += "|" + layer_name;

			std::string node_name =
				"multilight_layer_" + std::to_string(node_names.size()+1);
			node_names.push_back(node_name);
			OP_Node *layer_node =
				multilight_vars->createNode("rendervar", node_name.c_str());

			layer_node->setString("color3f", CH_STRING_LITERAL,
				"dataType", 0, 0.0f);
			layer_node->setString(aov_name.c_str(), CH_STRING_LITERAL,
				"xn__driverparametersaovname_jebkd", 0, 0.0f);
			layer_node->setString(source_name.c_str(), CH_STRING_LITERAL,
				"sourceName", 0, 0.0f);
			layer_node->setString(
				"color3h", CH_STRING_LITERAL,
				"xn__driverparametersaovformat_shbkd", 0, 0);

			lop_utilities::ConnectInput(layer_node, prev_node);
			prev_node = layer_node;
		}
	}

	OP_Node *output = multilight_vars->findNode("Output");
	lop_utilities::ConnectInput(output, prev_node);
	lop_utilities::LayoutNodes(multilight_vars);

	/* Update orderedVars. This needs a better implementation too. */
	LOP_Node *render_product = CAST_LOPNODE(findNode("renderproduct"));

	std::string ordered_vars_str(getRenderProductOrderedVarsString());
	for ( size_t i = 0; i < node_names.size(); i++)
	{
		ordered_vars_str += " /Render/Products/Vars/" + node_names[i];
	}
	render_product->setString(ordered_vars_str.c_str(), CH_STRING_LITERAL,
		"orderedVars", 0, 0.0f);
}

PRM_Template* LOP_3DelightRenderSettings::GetTemplates()
{
	static std::vector<PRM_Template> templates;
	if( !templates.empty() )
	{
		/* This should only be called once. But it is static content anyway so
		   we return it again just in case. */
		assert(false);
		return templates.data();
	}

	{
		static PRM_Name name("execute_render", "          Render          ");
		templates.emplace_back(
			PRM_CALLBACK, 1, &name, nullptr, nullptr, nullptr, &RenderCB);
	}

	{
		static PRM_Default def(0, "/Render/rendersettings");
		templates.emplace_back(
			PRM_STRING, 1, &lopPrimPathName, &def, &lopPrimPathMenu,
			nullptr, PRM_Callback{}, &lopPrimPathSpareData);
	}
	{
		static PRM_Name name("camera", "Camera Path");
		static PRM_Default def(0, "/cameras/camera1");
		templates.emplace_back(
			PRM_STRING, 1, &name, &def, nullptr, nullptr, PRM_Callback{},
			&lopPrimPathSpareData);
	}
	{
		static PRM_Name name("resolution", "Resolution");
		templates.emplace_back(PRM_INT, 2, &name);
	}

    std::vector<PRM_Template> shadingComponentLayers;
    std::vector<PRM_Template> auxiliaryVariableLayers;
	{
		const auto& descriptions = aov::getDescriptions();
		static std::vector<PRM_Name> names;
		/* Necessary so objects don't move around as we add more. */
		names.reserve(descriptions.size());
		for (const auto& desc : descriptions)
		{
			std::string parm_name = desc.m_variable_name;
			std::replace(parm_name.begin(), parm_name.end(), '.', '_');
			names.emplace_back(
				PRM_Name::COPY, parm_name.c_str(), desc.m_ui_name.c_str());
			PRM_Name* aovLayerName = &names.back();

			static PRM_Default s_true_default{1};
			PRM_Default *default_val = nullptr;
			/* Ci (the beauty pass) is enabled by default. */
			if( desc.m_variable_name == "Ci" )
				default_val = &s_true_default;

			if(desc.m_type == aov::e_shading)
			{
				shadingComponentLayers.emplace_back(
					PRM_TOGGLE, 1, aovLayerName, default_val);
			}
			else if (desc.m_type == aov::e_auxiliary)
			{
				auxiliaryVariableLayers.emplace_back(
					PRM_TOGGLE, 1, aovLayerName, default_val);
			}
		}
	}

	std::vector<PRM_Template> customVariableLayers;
	{
		const char *help = "Enter the name of an AOV defined by the AOV Group "
			"shader node.";
		static std::vector<PRM_Name> names;
		/* Necessary so objects don't move around as we add more. */
		names.reserve(kNumCustomAOVSlots);
		for( unsigned i = 1; i <= kNumCustomAOVSlots; ++i )
		{
			std::string number = std::to_string(i);
			std::string parm_name = "custom_aov_" + number;
			std::string parm_label = "Output Variable " + number;
			names.emplace_back(PRM_Name::COPY, parm_name, parm_label);
			customVariableLayers.emplace_back(
				PRM_STRING, 1, &names.back(), nullptr, nullptr, nullptr,
				PRM_Callback{}, nullptr, 1, help);
		}
	}

	/* Top level tabs definition. */
	{
		static PRM_Name name("top_level_tabs");
		static std::array<PRM_Default, 3> tabs =
		{
			PRM_Default(8, "Output"),
			PRM_Default(2, "Image Layers"),
			PRM_Default(1, "Overrides"),
		};
		templates.emplace_back(
			PRM_SWITCHER, int(tabs.size()), &name, tabs.data());
	}

	/* Output tab begins here. */
	{
		static PRM_Name name("display_rendered_images", "Display");
		static PRM_Default def(true);
		templates.emplace_back(
			PRM_TOGGLE, 1, &name, &def, nullptr, nullptr, &OutputRadioCallback);
	}
	{
		static PRM_Name name("save_rendered_images", "Image File");
		static PRM_Default def(false);
		templates.emplace_back(
			PRM_TOGGLE, 1, &name, &def, nullptr, nullptr, &OutputRadioCallback);
	}
	{
		static PRM_Name name("export_nsi_file", "NSI File");
		static PRM_Default def(false);
		templates.emplace_back(
			PRM_TOGGLE, 1, &name, &def, nullptr, nullptr, &OutputRadioCallback);
	}
	{
		static PRM_Name name("output_separator1", "");
		templates.emplace_back(PRM_SEPARATOR, 0, &name);
	}
	{
		static PRM_Name name("image_filename", "Image Filename");
		static PRM_Default def(0.0f, "$HIP/render/`$HIPNAME`_`$OS`_$F4.exr");
		templates.emplace_back(
			PRM_FILE, 1, &name, &def, nullptr, nullptr, nullptr,
			&PRM_SpareData::fileChooserModeWrite);
	}
	{
		static PRM_Name name("image_format", "Image Format");
		static PRM_Default def(0.0f, "exr");
		static PRM_Item formats[] =
		{
			PRM_Item("exr", "OpenEXR"),
			PRM_Item("deepexr", "OpenEXR (deep)"),
			PRM_Item("deepalphaexr", "OpenEXR (deep alpha only)"),
			PRM_Item("dwaaexr", "OpenEXR (DWAA)"),
			PRM_Item("deepalphadwaaexr", "OpenEXR (DWAA+deep alpha)"),
			PRM_Item()
		};
		static PRM_ChoiceList format_choices(PRM_CHOICELIST_SINGLE, formats);
		templates.emplace_back(
			PRM_STRING, 1, &name, &def, &format_choices);
	}
	{
		static PRM_Name name("output_separator2", "");
		templates.emplace_back(PRM_SEPARATOR, 0, &name);
	}
	{
		static PRM_Name name("nsi_filename", "NSI Filename");
		static PRM_Default def(0.0f, "$HIP/render/`$HIPNAME`_`$OS`_$F4.nsi");
		templates.emplace_back(
			PRM_FILE, 1, &name, &def, nullptr, nullptr, nullptr,
			&PRM_SpareData::fileChooserModeWrite);
	}

	/* Image Layers tab begins here. */
	{
		static PRM_Name name("image_subtabs");
		static std::vector<PRM_Default> tabs =
		{
			PRM_Default(shadingComponentLayers.size(), "Shading Components"),
			PRM_Default(auxiliaryVariableLayers.size(), "Auxiliary Variables"),
			PRM_Default(customVariableLayers.size(), "Custom Variables"),
		};

		templates.emplace_back(
			PRM_SWITCHER, int(tabs.size()), &name, tabs.data());

		templates.insert(templates.end(),
			shadingComponentLayers.begin(), shadingComponentLayers.end());

		templates.insert(templates.end(),
			auxiliaryVariableLayers.begin(), auxiliaryVariableLayers.end());

		templates.insert(templates.end(),
			customVariableLayers.begin(), customVariableLayers.end());
	}
	{
		static PRM_Name light_sets(settings::k_light_sets, "Light Sets");
		static PRM_Name use_light_set(settings::k_use_light_set, "");
		static PRM_Default use_light_set_d(false);
		static PRM_Name light_set(settings::k_light_set, "Light Set");
		static PRM_Default light_set_d(0.0, "");
		static PRM_Name use_rgba_only_set(settings::k_use_rgba_only_set, "");
		static PRM_Default use_rgba_only_set_d(false);
		static PRM_Default nb_lights(1);

		static PRM_Name multilight_selector(settings::k_multi_light_selection, "Render Multi-Light");
		static PRM_Default multilight_selector_d(0.0f, "off");
		static PRM_Item multilight_selector_i[] =
		{
			PRM_Item("off", "Off"),
			PRM_Item("tagged", "Tagged"),
			PRM_Item()
		};
		static PRM_ChoiceList multilight_selector_c(PRM_CHOICELIST_SINGLE, multilight_selector_i);

		static PRM_Name multilight_titles[] = {
			PRM_Name("ml_titles1", "Layer Name"),
			PRM_Name("ml_titles2", ""),
			PRM_Name("ml_titles3", ""),
			PRM_Name("ml_titles4", "RGBA only")
		};
		static PRM_Template light_set_templates[] =
		{
			PRM_Template(PRM_TOGGLE | PRM_TYPE_LABEL_NONE | PRM_TYPE_JOIN_NEXT, 1, &use_light_set, &use_light_set_d),
			PRM_Template(PRM_STRING | PRM_TYPE_LABEL_NONE | PRM_TYPE_JOIN_NEXT, 1, &light_set, &light_set_d),
			PRM_Template(PRM_TOGGLE | PRM_TYPE_LABEL_NONE, 1, &use_rgba_only_set, &use_rgba_only_set_d),
			PRM_Template()
		};
		static PRM_Name refresh_lights("refresh_lights", "Refresh");
		static PRM_Name dummy("dummy", "");
		static std::vector<PRM_Template> multilight_templates =
		{
			PRM_Template(PRM_ORD, 1, &multilight_selector, &multilight_selector_d, &multilight_selector_c),
			PRM_Template(PRM_LABEL | PRM_TYPE_JOIN_NEXT, 1, &multilight_titles[0]),
			PRM_Template(PRM_LABEL | PRM_TYPE_JOIN_NEXT, 1, &multilight_titles[1]),
			PRM_Template(PRM_LABEL | PRM_TYPE_JOIN_NEXT, 1, &multilight_titles[2]),
			PRM_Template(PRM_LABEL, 1, &multilight_titles[3]),
			PRM_Template(PRM_MultiType(PRM_MULTITYPE_SCROLL | PRM_MULTITYPE_NO_CONTROL_UI), light_set_templates, settings::k_one_line * 6.0f, &light_sets, &nb_lights),
			PRM_Template(PRM_CALLBACK | PRM_TYPE_JOIN_NEXT, 1, &refresh_lights, 0, 0, 0, &refreshMultiLightCB),
			PRM_Template(PRM_LABEL, 1, &dummy),
		};

		static PRM_Name tab_name("multilight_tab");
		static std::vector<PRM_Default> tabs =
		{
			PRM_Default(multilight_templates.size(), "Multi-Light")
		};

		templates.push_back(
			PRM_Template(PRM_SWITCHER, tabs.size(), &tab_name, &tabs[0]));
		templates.insert(
			templates.end(),
			multilight_templates.begin(),
			multilight_templates.end());
	}

	/* Overrides tab begins here. */
	{
		static PRM_Name name("enable_depth_of_field", "Enable Depth of Field");
		static PRM_Default def(true);
		templates.emplace_back(PRM_TOGGLE, 1, &name, &def);
	}

    templates.push_back(PRM_Template());

    return templates.data();
}

void
LOP_3DelightRenderSettings::Register(OP_OperatorTable* io_table)
{
    OP_Operator *op = new OP_Operator(
        "3delightrendersettings",
        "3Delight Render Settings",
        LOP_3DelightRenderSettings::alloc,
        LOP_3DelightRenderSettings::GetTemplates(),
        LOP_SubNet::theChildTableName,
        0, 1, nullptr,
        OP_FLAG_GENERATOR | OP_FLAG_NETWORK);

    op->setIconName("ROP_3Delight");
    io_table->addOperator(op);
}

OP_Node *
LOP_3DelightRenderSettings::alloc(
	OP_Network *net, const char *name, OP_Operator *op)
{
    return new LOP_3DelightRenderSettings(net, name, op);
}

const char *
LOP_3DelightRenderSettings::getChildType() const
{
    return OPtypeIdToString(LOP_OPTYPE_ID);
}

OP_OpTypeId
LOP_3DelightRenderSettings::getChildTypeID() const
{
    return LOP_OPTYPE_ID;
}

LOP_3DelightRenderSettings::LOP_3DelightRenderSettings(
	OP_Network *net, const char *name, OP_Operator *op)
:
	LOP_SubNet(net, name, op)
{
}

LOP_3DelightRenderSettings::~LOP_3DelightRenderSettings()
{
}

bool
LOP_3DelightRenderSettings::runCreateScript()
{
	bool ret = LOP_SubNet::runCreateScript();

	//Create necessary nodes for our network.
	OP_Node *lop_render_var = createNode("3delightstandardrendervars");
	OP_Node *custom_vars = createCustomVarsNetwork();
	OP_Node *multilight_vars = createMultilightVarsNetwork();
	OP_Node *render_product = createNode("renderproduct", "renderproduct");
	OP_Node *stream_product = createNode("renderproduct", "stream_renderproduct");
	OP_Node *render_settings = createNode("rendersettings", "rendersettings");
	OP_Node *output = createNode("output", "Output");
	OP_Node *usdrender_rop = createNode("usdrender_rop", "usdrender_rop");

	lop_render_var->runCreateScript();
	render_product->runCreateScript();
	stream_product->runCreateScript();
	render_settings->runCreateScript();
	output->runCreateScript();
	usdrender_rop->runCreateScript();

    //Set node's parameters. Fix this!
    setInt("resolution",0,0,1280);
    setInt("resolution",1,0,720);

    //set rendersettings parameters from 3Delight LOPRenderSettings node.
	render_settings->setString(
		"chs(\"../primpath\")", CH_OLD_EXPRESSION, "primpath", 0, 0);
    render_settings->setString("chs(\"../camera\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"camera",0,0);
    render_settings->setString("chs(\"../resolution1\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"resolution",0,0);
    render_settings->setString("chs(\"../resolution2\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"resolution",1,0);
    render_settings->setString("chs(\"../shading_samples\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalshadingsamples_gebg",0,0);
    render_settings->setString("chs(\"../pixel_samples\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalpixelsamples_69ag",0,0);
    render_settings->setString("chs(\"../volume_samples\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalvolumesamples_tcbg",0,0);
    render_settings->setString("chs(\"../max_diffuse_depth\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalmaximumdiffusedepth_lmbg",0,0);
    render_settings->setString("chs(\"../max_reflection_depth\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalmaximumreflectiondepth_hrbg",0,0);
    render_settings->setString("chs(\"../max_refraction_depth\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalmaximumrefractiondepth_hrbg",0,0);
    render_settings->setString("chs(\"../max_hair_depth\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalmaximumhairdepth_phbg",0,0);
    render_settings->setString("chs(\"../max_distance\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalmaximumdistance_2fbg",0,0);

    //Set NSI parameters on usd rendersettings child node. Allows these parameters to be exported by usd.
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalshadingsamples_control_hrbg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalpixelsamples_control_7nbg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalvolumesamples_control_upbg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalmaximumdiffusedepth_control_mzbg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalmaximumreflectiondepth_control_i4bg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalmaximumrefractiondepth_control_i4bg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalmaximumhairdepth_control_qubg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalmaximumdistance_control_3sbg",0,0);

	/* The main image product is always rendered but the stream product depends
	   on "NSI File" being selected. */
	render_settings->setString(
		"/Render/Products/renderproduct"
		" `ifs(ch(\"../export_nsi_file\"), "
			"\"/Render/Products/stream_renderproduct\", \"\")`",
		CH_STRING_LITERAL, "products", 0, 0);

	/* Connect overrides to rendersettings.*/
	render_settings->setString("set", CH_STRING_LITERAL,
		"xn__nsiglobalenabledepthoffield_control_zxbg", 0, 0);
	render_settings->setString(
		"ch(\"../enable_depth_of_field\")", CH_OLD_EXPRESSION,
		"xn__nsiglobalenabledepthoffield_ykbg", 0, 0);

	/* Set all AOVs in orderedVars parameter of renderproduct node.
		FIXME: Maybe a better solution can be implemented? */
	render_product->setString(getRenderProductOrderedVarsString().c_str(),
		CH_STRING_LITERAL,"orderedVars",0,0);

	/* Set image file name. */
	render_product->setString(
		"chs(\"../image_filename\")", CH_OLD_EXPRESSION, "productName", 0, 0);

	/*
		Change product type based on "Display" toggle.
		Use "raster" when there is no ROP so AOVs are available in the viewport.
	*/
	render_product->setString(
		"ifs(0 == strcmp(contextoptions('ropname'), ''), 'raster', "
			"ifs(ch('../display_rendered_images'), 'nsi:display', "
				"strcat('nsi:', chs('../image_format'))))",
		CH_OLD_EXPRESSION, "productType", 0, 0);

	/* Setup the nsi stream renderproduct. */
	stream_product->setString(
		"chs(\"../nsi_filename\")", CH_OLD_EXPRESSION, "productName", 0, 0);
	stream_product->setString(
		"nsi:apistream", CH_STRING_LITERAL, "productType", 0, 0);

	/* Configure the usdrender node for 3Delight. */
	usdrender_rop->setString(
		"HdNSIRendererPlugin", CH_STRING_LITERAL, "renderer", 0, 0);
	usdrender_rop->setString(
		"chs(\"../rendersettings/primpath\")",
		CH_OLD_EXPRESSION, "rendersettings", 0, 0);

	/* Connect out internal network. */
	lop_utilities::ConnectInput(lop_render_var, nullptr);
	custom_vars->setInput(0, lop_render_var);
	multilight_vars->setInput(0, custom_vars);
	render_product->setInput(0, multilight_vars);
	stream_product->setInput(0, render_product);
	render_settings->setInput(0, stream_product);
	output->setInput(0, render_settings);
	usdrender_rop->setInput(0, render_settings);

	lop_utilities::LayoutNodes(this);
    finishedLoadingNetwork();
	return ret;
}

/*
	Trigger the Render button of our usdrender_rop node.
*/
void LOP_3DelightRenderSettings::DoRender(double i_time)
{
	OP_Node *usdrender = getChild("usdrender_rop");
	if( !usdrender )
		return;

	PRM_Parm *parm = usdrender->getParmPtr("execute");
	if( !parm )
		return;

	usdrender->triggerParmCallback(parm, i_time, 0, usdrender);
}

void
LOP_3DelightRenderSettings::PRIMPATH(UT_String &str, fpreal t)
{
    evalString(str, lopPrimPathName.getToken(), 0, t);
    HUSDmakeValidUsdPath(str, true);
}

void
LOP_3DelightRenderSettings::getCamera(UT_String &str, fpreal t)
{
    evalString(str, k_camera, 0, t);
    HUSDmakeValidUsdPath(str, true);
}

OP_ERROR
LOP_3DelightRenderSettings::cookMyLop(OP_Context &context)
{
	if (cookModifyInput(context) >= UT_ERROR_FATAL)
	{
		return error();
	}

	updateMultiLight();

#if HDK_API_VERSION >= 19050000
    syncDelayedOTL();
    UT_ASSERT(!getDelaySyncOTL());
#endif
    return cookReferenceNode(context, getNodeForSubnetOutput(0));
}

bool
LOP_3DelightRenderSettings::updateParmsFlags()
{
	bool changed = OP_Network::updateParmsFlags();

	UT_String multi_light_selection_val;
	evalString(multi_light_selection_val, "multi_light_selection", 0, 0.0f);
	bool enable_multi_light_list = multi_light_selection_val == "selection";

	PRM_Parm& multilight_parm = getParm(settings::k_light_sets);
	int multilight_size = multilight_parm.getMultiParmNumItems();
	for (int i = 0; i < multilight_size; i++)
	{
		changed |= enableParm(settings::GetLightToken(i), false);
		changed |= enableParm(settings::GetUseRBBAOnlyToken(i),
			enable_multi_light_list && evalInt(settings::GetUseLightToken(i), 0, 0.0f));
		changed |= enableParm(settings::GetUseLightToken(i), enable_multi_light_list);
	}

	return changed;
}

OP_Node* LOP_3DelightRenderSettings::createCustomVarsNetwork()
{
	LOP_Node *net = CAST_LOPNODE(createNode("subnet", "customvars"));

	OP_Node *prev_node = nullptr;
	for( unsigned i = 1; i <= kNumCustomAOVSlots; ++i )
	{
		std::string node_name = "custom_aov" + std::to_string(i);
		std::string parm_name = "../../custom_aov_" + std::to_string(i);
		OP_Node *aov_node = net->createNode("rendervar", node_name.c_str());
		aov_node->runCreateScript();

		std::string source_expr =
			"strcat(\"shader:\", chs(\"" + parm_name + "\"))";
		aov_node->setString(
			source_expr.c_str(), CH_OLD_EXPRESSION, "sourceName", 0, 0);
		std::string name_expr = "chs(\"" + parm_name + "\")";
		aov_node->setString(
			name_expr.c_str(), CH_OLD_EXPRESSION,
			"xn__driverparametersaovname_jebkd", 0, 0);
		LOP_3DelightStandardRenderVars::setRenderVarPrefix(
			aov_node, name_expr.c_str(), CH_OLD_EXPRESSION);
		aov_node->setString(
			"color3f", CH_STRING_LITERAL, "dataType", 0, 0);
		aov_node->setString(
			"color3h", CH_STRING_LITERAL,
			"xn__driverparametersaovformat_shbkd", 0, 0);

		lop_utilities::ConnectInput(aov_node, prev_node);
		prev_node = aov_node;
	}

	OP_Node *output = net->createNode("output", "Output");
	lop_utilities::ConnectInput(output, prev_node);

	lop_utilities::LayoutNodes(net);
	return net;
}

OP_Node* LOP_3DelightRenderSettings::createMultilightVarsNetwork()
{
	LOP_Node *net = CAST_LOPNODE(createNode("subnet", "multilightvars"));

	OP_Node *output = net->createNode("output", "Output");
	lop_utilities::ConnectInput(output, nullptr);

	return net;
}
