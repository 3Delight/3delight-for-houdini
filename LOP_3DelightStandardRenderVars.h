
#ifndef __LOP_3DelightStandardRenderVars_h__
#define __LOP_3DelightStandardRenderVars_h__
#include <LOP/LOP_SubNet.h>

class LOP_3DelightStandardRenderVars : public LOP_SubNet
{
public:
    static void Register(OP_OperatorTable* io_table);

    LOP_3DelightStandardRenderVars(OP_Network *net, const char *name, OP_Operator *op);
    ~LOP_3DelightStandardRenderVars() override;
    static PRM_Template  myTemplateList[];
    static OP_Node *alloc(OP_Network *net, const char *name,
                                OP_Operator *op);
    const char *getChildType() const override;
    OP_OpTypeId getChildTypeID() const override;

    /**
		Called when a new node is created.
	*/
	virtual bool runCreateScript() override;
	static PRM_Template* GetTemplates();

	static void setRenderVarPrefix(
		OP_Node *render_var_node,
		const UT_StringRef &val,
		CH_StringMeaning meaning);

public:

protected:
    /// Method to cook USD data for the LOP
    OP_ERROR cookMyLop(OP_Context &context) override;
private:
};
#endif
