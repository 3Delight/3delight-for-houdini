#ifndef __lop_utilities
#define __lop_utilities

#include <string>
#include <unordered_map>
#include <vector>

class OP_Network;
class OP_Node;
class LOP_Node;
class HUSD_FindPrims;
class HUSD_AutoLayerLock;

namespace lop_utilities
{
void LayoutNodes(OP_Network *net);
void ConnectInput(OP_Node *node, OP_Node *input_node);
void FindLights(LOP_Node *i_node, HUSD_AutoLayerLock &i_layer_lock,
	std::vector<std::string> &o_light_primpaths);
void FindLPETags(
	LOP_Node *i_node, HUSD_AutoLayerLock &i_layer_lock,
	std::unordered_map<std::string, std::string> &o_primpath_tags);
}

#endif
