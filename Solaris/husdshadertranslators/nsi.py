# This is the module which handles the translation 3Delight for Houdini's
# shader VOP nodes to USD shaders.
import hou
if hou.applicationVersion()[0] >= 19:
    from husd.shadertranslator \
        import ShaderTranslator as TranslatorBase
    from husd.shadertranslator \
        import ShaderTranslatorHelper as HelperBase
    from husd.shadertranslator \
        import MultiParmTranslator as MultiParmTranslator
    from husd.shadertranslator \
        import RampParmTranslator as RampParmTranslator
    from husd \
        import shaderutils as shaderutils
else:
    from husdshadertranslators.default \
        import DefaultShaderTranslator as TranslatorBase
    from husdshadertranslators.default \
        import DefaultShaderTranslatorHelper as HelperBase
    from husdshadertranslators.default \
        import MultiParmTranslator as MultiParmTranslator
    from husdshadertranslators.default \
        import RampParmTranslator as RampParmTranslator
    from husdshadertranslators \
        import utils as shaderutils

from pxr import UsdShade, Sdf

class NSIRampParmTranslator(RampParmTranslator):
    """
    Custom parm translator for Houdini ramps.
    Converts interpolation to the int array we expect instead of the single
    string of the default translator.
    """
    def createBasisAttrib(self, usd_shader):
        return shaderutils.createUsdInputAttrib(
            usd_shader, self.basisAttribName(),
            Sdf.ValueTypeNames.Int.arrayType)

    def rampBasisValue(self, ramp_val):
        # See osl_utilities::ramp::FromHoudiniInterpolation() in the C++
        basisMap = {
            hou.rampBasis.Constant: 0,
            hou.rampBasis.Linear: 1,
            hou.rampBasis.MonotoneCubic: 2,
            hou.rampBasis.CatmullRom: 3,
            hou.rampBasis.Bezier: 3,
            hou.rampBasis.BSpline: 3,
            hou.rampBasis.Hermite: 3,
        }
        return [basisMap.get(x, 1) for x in ramp_val.basis()]


class AOVGroupParmTranslator(MultiParmTranslator):
    """
    Custom parm translator for AOVGroup's aovs parameter.
    """
    def __init__(self, parm_tuple):
        MultiParmTranslator.__init__(self, parm_tuple)

    def attribName(self):
        """
        Rename the aovs parameter of the parent translator to what the shader
        expects, for the output as a string array.
        """
        # We could use the sidefx::shader_parmname tag on the parm but we need
        # this translator anyway so this works just as well.
        return 'colorAOVNames'

    def createAndSetAttrib(self, usd_shader, usd_time_code,
            metadata_infos = []):
        """
        Export the default color values together with the AOV names.

        Note that the colors exist as VOP inputs but not as parms so no value
        gets exported under any name without this.
        """
        super().createAndSetAttrib(usd_shader, usd_time_code, metadata_infos)
        # Also produce colorAOVValues attribute to set the array size.
        usd_attrib = shaderutils.createUsdInputAttrib(
            usd_shader, 'colorAOVValues',
            shaderutils.sdfTypeFromVopTypeName('color').arrayType)
        usd_time_code = self.adjustedTimeCode(usd_time_code)
        # A list of black colors, as long as the string array.
        parm_value = [(0,0,0) for x in self.evalParm(usd_time_code)]
        shaderutils.setUsdAttribFromParmValue(
            usd_attrib, parm_value, usd_time_code)


class NSIShaderTranslatorHelper(HelperBase):
    def __init__(self, translator_id, usd_stage, usd_material_path,
            usd_time_code):
        # Will contain maps to rename individual aov input parameters.
        # Start with an empty map as it will get used if createUsdShaderInput()
        # is not called through createUsdShaderChain().
        self._parameterMappingStack = [{}]
        super().__init__(translator_id, usd_stage, usd_material_path,
            usd_time_code)

    def _createTerminalShader(self, terminal_node):
        # The default houdini translator exports the dlTerminal node which is
        # not what we want. Instead, create a separate network of the correct
        # type for every connected input of the terminal node.
        # This depends on correct typing of the node parameters in Houdini
        # (see VOP_ExternalOSL::GetVOPType()).
        terminal_to_shader_type = {
            'Surface': hou.shaderType.Surface,
            'Displacement': hou.shaderType.Displacement,
            'Volume': hou.shaderType.Atmosphere}
        for conn in terminal_node.inputConnections():
            if conn.outputName() in terminal_to_shader_type:
                shader_type = terminal_to_shader_type[conn.outputName()]
                self.createAndConnectUsdTerminalShader(
                    conn.inputNode(), shader_type, conn.inputName())

    def _createMBShader(self, mb_node, requested_shader_type):
        # The default houdini translator attempts to translate the material
        # builder itself which is no good. This forwards translation to the
        # dlTerminal shader inside the 3Delight Material Builder.
        for child in mb_node.children():
            if child.type().name() == '3Delight::dlTerminal':
                return self._createTerminalShader(child)

    def createAndConnectUsdTerminalShader(self, shader_node, 
            requested_shader_type, shader_node_output_name):
        """ See husdshadertranslators.default """
        if shader_node.type().name() == '3Delight::dlMaterialbuilder':
            return self._createMBShader(shader_node, requested_shader_type)
        # This supports older scenes. This name is deprecated.
        if shader_node.type().name() == '3Delight::dlMaterialBuilder':
            return self._createMBShader(shader_node, requested_shader_type)
        if shader_node.type().name() == '3Delight::dlTerminal':
            return self._createTerminalShader(shader_node)
        return super(NSIShaderTranslatorHelper, self) \
            .createAndConnectUsdTerminalShader(shader_node,
                requested_shader_type, shader_node_output_name)

    def createUsdShaderID(self, usd_shader, shader_node):
        """
        Simplified version of the default translator helper

        Always sets an id, even if name looks like an asset.
        Our Hydra plugin will handle that correctly. It does not, however,
        handle shaders with sourceAsset which the default implementation
        produces when the name looks like a path.
        """
        if not usd_shader.GetPrim().IsA(UsdShade.Shader):
            return
        shader_name = shader_node.shaderNameForSignature()
        if not shader_name:
            return
        usd_shader.SetShaderId( shader_name )

    def rampParameterTranslator( self, parm_tuple ):
        return NSIRampParmTranslator(parm_tuple)

    def multiParameterTranslator( self, parm_tuple ):
        # Override the translator used for dlAOVGroup's 'aovs' multi parm.
        if parm_tuple.node().type().name() == 'dlAOVGroup' and \
            parm_tuple.name() == 'aovs':
            return AOVGroupParmTranslator(parm_tuple)
        return super(NSIShaderTranslatorHelper, self) \
            .multiParameterTranslator(parm_tuple)

    # The following two are to change the names of the input connection points
    # for the various AOVs of dlAOVGroup from the aov's name to an item in the
    # colorAOVValues array. Ideally we'd have the sidefx::shader_parmname tag
    # on them but as far as I can tell, they are made up connection points for
    # the shader network and don't actually have anywhere we can set tags.
    #
    # I could not find any nice place to do the name change which also has
    # access to the shader node so we can be certain we're on dlAOVGroup. So
    # I'm wrapping a top level call with access to the shader node, storing
    # state to remember we're in dlAOVGroup, and doing the name change in a
    # convenient low level call.
    #
    # There are definitely ways to get to createUsdShaderInput() which are not
    # covered by this. But so far, they don't appear relevant for us. I sure
    # wish the connections used something like the ParmTranslator.
    def createUsdShaderChain(self, shader_node, shader_node_output_name,
        usd_parent_connectable, usd_shader_prim_name):
        # Push a new parameter name map.
        mapping = {}
        self._parameterMappingStack.append(mapping)
        if shader_node.type().name() == 'dlAOVGroup':
            # Fetch the list of names and store in the map. Our Hydra plugin
            # will convert 'colorAOVValues:_x' into 'colorAOVValues[x]'
            # The former is needed to get a valid Usd identifier.
            idx = 0
            for aovs_p in shader_node.parm('aovs').multiParmInstances():
                mapping[aovs_p.eval()] = 'colorAOVValues:_' + str(idx)
                idx += 1
        # Run the translation code.
        r = super().createUsdShaderChain(shader_node, shader_node_output_name,
            usd_parent_connectable, usd_shader_prim_name)
        # Unstack this shader's parameter name map.
        self._parameterMappingStack.pop()
        return r

    def createUsdShaderInput(self,
            usd_shader, usd_shader_input_name, usd_shader_input_type):
        # This override applies the parameter name map defined in the previous
        # function.
        usd_shader_input_name = self._parameterMappingStack[-1].get(
            usd_shader_input_name, usd_shader_input_name)
        return super().createUsdShaderInput(
            usd_shader, usd_shader_input_name, usd_shader_input_type)


class NSIShaderTranslator(TranslatorBase):
    def matchesRenderMask(self, render_mask):
        """ See husdshadertranslators.default """
        return render_mask == 'nsi'

    def shaderTranslatorHelper(self, translator_id, usd_stage,
            usd_material_path, usd_time_code):
        """ Return our own translator helper. """
        return NSIShaderTranslatorHelper(translator_id,
            usd_stage, usd_material_path, usd_time_code)

################################################################################
# In order to be considered for shader translation, this python module
# implements the factory function that returns a translator object.
# See husd.shadertranslator.usdShaderTranslator() for details.
nsi_translator = NSIShaderTranslator()
def usdShaderTranslator():
    return nsi_translator

# vim: set softtabstop=4 expandtab shiftwidth=4:
