#include "lop_utilities.h"

#include <OP/OP_Layout.h>
#include <OP/OP_Network.h>
#include <OP/OP_SubnetIndirectInput.h>
#include <LOP/LOP_Node.h>
#include <HUSD/HUSD_FindPrims.h>
#include <HUSD/HUSD_GetAttributes.h>
#include <HUSD/HUSD_Info.h>
#include <HUSD/HUSD_Path.h>

/*
	Run automatic layout on all nodes in the network.
*/
void lop_utilities::LayoutNodes(OP_Network *net)
{
	OP_Layout layout(net);
	/* Get indirect inputs. */
	int ni = net->getNparentInputs();
	for( int i = 0; i < ni; ++i )
	{
		layout.addLayoutItem(net->getParentInput(i));
	}
	/* Get nodes. */
	int nc = net->getNchildren();
	for( int i = 0; i < nc; ++i )
	{
		layout.addLayoutItem(net->getChild(i));
	}
	layout.layoutOps(OP_LAYOUT_TOP_TO_BOT, nullptr);
}

/*
	Connect node's first input to input_node's first output.
	If input_node is nullptr, connect to parent's first indirect input instead.
*/
void lop_utilities::ConnectInput(OP_Node *node, OP_Node *input_node)
{
	if( input_node )
	{
		node->setInput(0, input_node);
	}
	else
	{
		OP_SubnetIndirectInput *parent_input =
			node->getParentNetwork()->getParentInput(0);
		if( parent_input )
		{
			node->setIndirectInput(0, parent_input);
		}
	}
}

void lop_utilities::FindLights(
	LOP_Node *i_node, HUSD_AutoLayerLock &i_layer_lock,
	std::vector<std::string> &o_light_primpaths)
{
	HUSD_FindPrims findPrims(i_layer_lock);
	if (!LOP_Node::getSimplifiedCollection(i_node, "*", findPrims))
	{
		return;
	}

	HUSD_Info info(i_layer_lock);

	auto pathSet = findPrims.getExpandedPathSet();
	for ( const auto &path : pathSet )
	{
		const UT_StringHolder &pathStr = path.pathStr();
		UT_StringHolder primType = info.getPrimType(pathStr);
		// FIXME: Detect all lights by type name.
		if (primType.toStdString().rfind("Light") != std::string::npos)
		{
			o_light_primpaths.push_back(pathStr.toStdString());
		}
	}
}

void lop_utilities::FindLPETags(
	LOP_Node *i_node, HUSD_AutoLayerLock &i_layer_lock,
	std::unordered_map<std::string, std::string> &o_primpath_tags)
{
	HUSD_FindPrims findPrims(i_layer_lock);
	if (!LOP_Node::getSimplifiedCollection(i_node, "*", findPrims))
	{
		return;
	}

	auto pathSet = findPrims.getExpandedPathSet();
	for (const auto &path : pathSet)
	{
		const UT_StringHolder &pathStr = path.pathStr();
		HUSD_GetAttributes ga(i_layer_lock);
		UT_StringHolder val;
		if (ga.getAttribute(pathStr, "karma:light:lpetag", val, HUSD_TimeCode())
			&& !val.isEmpty())
		{
			o_primpath_tags.emplace(pathStr.c_str(), val.c_str());
		}
	}
}
